import { Context } from "koa";
import { IMiddleware } from "koa-router";


export async function index(ctx: Context, next: any) {
    ctx.body = 'Hello World!';
    await next();
}

export async function getTest(ctx: Context, next: any) {
    ctx.response.body = JSON.stringify({data: { result: 'OK'}});
    ctx.response.status = 200;
    ctx.response.type = 'application/json'
    await next();
}

export async function postTest(ctx: Context, next: any) {
    ctx.response.body = JSON.stringify({data: ctx.request.body});
    ctx.response.status = 200;
    ctx.response.type = 'application/json'
    await next();
}