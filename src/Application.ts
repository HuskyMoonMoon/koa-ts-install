import * as Koa from 'koa';
import * as Router from 'koa-router';
import { IMiddleware } from 'koa-router';
import * as BodyParser from 'koa-bodyparser';
import RouteConfig from './routes/_RouteConfig';
import Configuration from './Configuration';
import VerboseLog from './utils/VerboseLog'
<<<<<<< Updated upstream
import Routes from './routes/_RouteConfig';
=======
import TestControllerRoute from './routes/TestControllerRoute';
>>>>>>> Stashed changes

export class Application {
    private koa: Koa = new Koa();
    constructor() {
        this.registerMiddleware();
    }

    private registerMiddleware(): void {        
        this.koa.use(this.bodyParserMiddleware);
        this.koa.use(Routes.routes());
        this.koa.use(VerboseLog);
    }

    public start(port: number): void {
        this.koa.listen(port);
        console.log('Server running on port ' + port);
        console.log('Root url is ' + Configuration.app.rootUrl);
    }
    
    private async bodyParserMiddleware(ctx: Koa.Context, next: any) {
        ctx.app.use(BodyParser());
        await next();
    }
}