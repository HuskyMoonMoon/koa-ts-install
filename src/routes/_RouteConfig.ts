import * as Router from "koa-router";
import Configuration from "../Configuration";
import * as TestController from "../controllers/TestController";
import { TestControllerRoute } from './TestControllerRoute';

export const Routes = new Router()
    .prefix(Configuration.app.rootUrl)
    .get('/', TestController.index)
    .use('/test', TestControllerRoute.routes());
export default Routes;