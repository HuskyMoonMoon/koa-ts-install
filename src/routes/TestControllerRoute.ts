import * as Router from 'koa-router'
import * as TestController from '../controllers/TestController'

export const TestControllerRoute = new Router()
    .get('/', TestController.getTest)
    .post('/', TestController.postTest);

export default TestControllerRoute;