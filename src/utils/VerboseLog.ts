import { IMiddleware } from 'koa-router';
import { Context } from 'koa';

export async function VerboseLog(ctx : Context, next : any) {
    console.log('-------------------Request---------------------');
    console.log('Url:', ctx.url);
    console.log('Header:\n', ctx.headers);
    console.log('Method:', ctx.method);
    console.log('Body:\n', ctx.body);
    console.log('-------------------Response---------------------');
    console.log('Header:\n', ctx.response.header);
    console.log('Body:\n', ctx.response.body);
    console.log('Type: ', ctx.response.type);
    console.log('===============================================');
    await next();
}

export default VerboseLog;